﻿
global using static Raylib_CsLo.RayGui;
using Raylib_CsLo;
namespace Platformia
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            // Steam Deck screen resolution
            int screenWidth = 1280;
            int screenHeight = 800;
            string fPath = "/home/deca/programming/platformia/PNG/Players/128x256/Green/alienGreen_stand.png";
            string animPath = "/home/deca/programming/platformia/PNG/Players/128x256/Green/";


            SetTraceLogLevel((int)TraceLogLevel.LOG_ERROR);
            InitWindow(screenWidth, screenHeight, "Platformia");
            Player player = new Player(fPath,animPath, screenWidth / 2.0, screenHeight / 2.0);
            GuiLoadStyleDefault();
            SetTargetFPS(30);

            while (!WindowShouldClose())
            {
                BeginDrawing();
                ClearBackground(Raylib.SKYBLUE);
                DrawFPS(10, 10);
                player.Update();
                player.Draw();
                DrawText("Test", 640, 360, 50, Raylib.RED);
                GuiButton(new Rectangle(350, 350, 100, 50), "#05#Test Button");
                EndDrawing();
            }
            Raylib.CloseWindow();
        }
    }
}