global using static Raylib_CsLo.Raylib;
using System.Numerics;
using System.Text.RegularExpressions;
using Raylib_CsLo;

namespace Platformia;
public enum Direction
    {
        Up,
        Down,
        Left,
        Right
    }

    public enum State
    {
        Idle,
        Moving
    }
public class Player
{
    private Direction currentDirection = Direction.Right;

    private State currentState = State.Idle;
    private Vector2 Pos;

    private readonly float Speed = 5.0f;

    // Properties
    private readonly Texture Tex;

    private readonly Color Tint = RAYWHITE;
    private Dictionary<string, List<string>> animationList;
    

    // Constructor
    public Player(string fPath, string animPath, double xPos, double yPos)
    {
        var image = LoadImage(fPath);
        //Image converted to texture, GPU memory (VRAM)
        Tex = LoadTextureFromImage(image);
        // Once image has been converted to texture and uploaded to VRAM, it can be unloaded from RAM 
        UnloadImage(image);
        Pos = new Vector2(40, 500 / 2.0f - 50);
        animationList = BuildActionHashMap(animPath);
    }

    public void Update()
    {
        // Change direction based on arrow key presses
        if (IsKeyDown(KeyboardKey.KEY_W))
        {
            currentDirection = Direction.Up;
            currentState = State.Moving;
        }
        else if (IsKeyDown(KeyboardKey.KEY_S))
        {
            currentDirection = Direction.Down;
            currentState = State.Moving;
        }
        else if (IsKeyDown(KeyboardKey.KEY_A))
        {
            currentDirection = Direction.Left;
            currentState = State.Moving;
        }
        else if (IsKeyDown(KeyboardKey.KEY_D))
        {
            currentDirection = Direction.Right;
            currentState = State.Moving;
        }
        else
        {
            currentState = State.Idle;
        }

        // Update position based on current direction
        if (currentState == State.Moving)
            switch (currentDirection)
            {
                case Direction.Up:
                    Pos.Y -= Speed;
                    break;
                case Direction.Down:
                    Pos.Y += Speed;
                    break;
                case Direction.Left:
                    Pos.X -= Speed;
                    break;
                case Direction.Right:
                    Pos.X += Speed;
                    break;
            }
    }


    public void Draw()
    {
        DrawTextureV(Tex, Pos, Tint);
    }

    public Dictionary<string, List<string>> BuildActionHashMap(string folderPath)
    {
        Dictionary<string, List<string>> actionHashMap = new Dictionary<string, List<string>>();

        string[] fileNames = Directory.GetFiles(folderPath, "*.png")
            .Select(filePath => Path.GetFileNameWithoutExtension(filePath))
            .ToArray();

        foreach (string fileName in fileNames)
        {
            string[] parts = fileName.Split('_');
            if (parts.Length >= 2)
            {
                // remove numbers in for example climb1, climb2
                string action = Regex.Replace(parts[1], "[0-9]", "");
                string fileNameNoNumber = Regex.Replace(fileName, "[0-9]", ""); 
                // reconstruct full path without number so it can be added
                // correctly to the array
                string completePath = Path.Combine(folderPath, fileNameNoNumber);
                if (!actionHashMap.ContainsKey(action))
                    actionHashMap[action] = new List<string>();
                // manually add the numbers so we don't have to sort
                if (actionHashMap[action].Count == 0)
                {
                actionHashMap[action].Add($"{completePath}1.png");
                }
                else
                {
                    int count = actionHashMap[action].Count;
                    actionHashMap[action].Add($"{completePath}{count+1}.png");
                }
            }
        }

        return actionHashMap;
    }
}

